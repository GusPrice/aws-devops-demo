Serverless Pipeline in GitLab

This is a sample pipeline in GitLab that deploys three serverless functions that are triggered by a chain of SNS Topics. Once deployed, you can trigger the first function, "Uno", by submitting an SNS Event, "Trigger Uno". From here you will see in the Cloudwatch logs that each Lambda function submits an SNS Event to the next triggering it. 

In order for this to work in your account, please follow these steps:

1. Clone this repository into your own GitLab repository
2. Set the following CI / CD Variables for your AWS account:
	a. aws_access_key_id
	b. aws_secret_access_key

Once you have this set up, any change in the repository, whether to the Lambda Function Python code or to the Terraform should trigger the GitLab pipeline to deploy in your AWS account.

If you need to run the Terraform locally, you will want to set the profile in terraform/vars/poc.vars to match your local aws profile. You'll need to initialize terraform and each of the modules, then run Terraform apply.


Initialize Terraform:
$ cd terraform/
$ terraform init
$ cd modules/lambda-function/
$ terraform init
$ cd ../s3-bucket/
$ terraform init

Run the Terraform:
$ cd ../../
$ terraform apply --var-file=params/poc.tfvars

